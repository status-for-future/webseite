# 16.4.2021

* [<](/2021/2021-04-09.html)

<div class="wa_tuerkis"></div>

📺 Netflix und YouTube verbrauchen viele Daten und damit Strom. 😨


Deshalb habe ich beide Dienste so eingestellt, dass Filme in niedriger Qualität gestreamt werden.
<br><br>
Bei Bedarf wechsle ich zu besserer Qualität.
<br><br>
Für Filmfreaks ist das natürlich nichts, aber mir persönlich reicht es meistens. 😊


Netflix überträgt bei bester Qualität ca. 3 GB/Stunde.
<br><br>
Die niedrigste Einstellung benötigt nur ein Zehntel ↘↘↘ ca. 300 MB/Stunde.


Ich spare also **90%** Datentransfer gegenüber der höchsten Qualitätsstufe &ndash; und damit indirekt Strom &ndash; auf dem Weg von Netflix zu meinem Rechner. 🥳



