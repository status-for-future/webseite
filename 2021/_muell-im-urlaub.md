# 20. August 2021

* [<](/2021/2021-07-30.html)

<div class="wa_flieder"></div>

Soundtrack heute:  
🎶 Too Much Food - Jason Mraz 🎸  
[https://www.youtube.com/watch?v=RR7wj_pAh-A](https://www.youtube.com/watch?v=RR7wj_pAh-A)

Im Urlaub ist mir der 🗑 Müll aufgefallen den wir so produziert haben. Wir waren in einer Ferienwohnung und konnten selber kochen.

Dabei entstand dieser Müll: 1 größerer Karton mit Papiermüll 📰, 1 kleiner Beutel gelber Sack und 1 Beutel Allgemeinmüll 🚯.

Ca. die Hälfte vom Allgemeinmüll war Biomüll, den man _\_leider\__ nicht gesondert entsorgen konnte.

Ist das viel?  
Ist das wenig?  
🤔
