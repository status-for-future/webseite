# 26.3.2021

* [<](/2021/2021-03-22.html)
* [>](/2021/2021-04-02.html)

<div class="wa_dunkelblau"></div>

Morgen abend 20:30 wird's für eine Stunde dunkel🕯.  <br>
  <br>
⏱ Eine Stunde für die 🌎 Erde &mdash; Earth Hour:  <br>
  <br>
[https://www.earthhour.org/take-part](https://www.earthhour.org/take-part)  <br>
  <br>
Machst du auch mit?




