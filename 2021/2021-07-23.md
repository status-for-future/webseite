# 23. Juli 2021

* [<](/2021/2021-07-16.html)
* [>](/2021/2021-07-30.html)

<div class="wa_oliv"></div>

Heutiger Soundtack:  
🎶 "The Big Picture" von Elton John 🎹
<br><br>
[https://youtu.be/CUyIaXe6nKs](https://youtu.be/CUyIaXe6nKs)


"Was kann ich denn schon tun? Ich arbeite doch nicht in einem "grünen" Beruf? Und ich kann das auch nicht einfach ändern, weil..."

Ich habe viel Zeit über diese Fragen gegrübelt.  
Sehr.  Viel.  Zeit.  
Inzwischen habe ich für mich ein paar Dinge herausgefunden.

Das Wichtigste was ich &mdash; was wir alle tun können, ist: *darüber reden*.

Warum? Weil das worüber wir sprechen unsere Aufmerksamkeit bekommt und damit wichtig wird.
<br><br>
Für mich hatte "Darüber reden" lustige Effekte...

<!-- TODO links -->
Beim Coding Festival von DATEV hält Tom Greenwood die Keynote zum Thema Nachhaltigkeit in der IT. 😁

Ich habe gemerkt, dass ich mit meinen Sorgen nicht allein bin und Menschen getroffen, die auch etwas ändern wollen.

"Darüber reden" hat auch dazu geführt, dass ich gemeinsam mit diesen Menschen einen Workshop halten werde mit dem Titel  
"Mein Auto braucht 8l/100km. Wie viel Sprit braucht deine Software?"




