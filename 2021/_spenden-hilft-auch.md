# 11.6.2021

* [<](/2021/2021-06-04.html)

<div class="wa_flieder"></div>


![Buchtitel: Klimawende von unten. Wie wir durch direkte Demokratie die Klimapolitik in die Hand nehmen.](/2021/buch-klimawendevonunten.jpg)

Vor kurzem ist mir dieses Buch von [klimawende.org](https://klimawende.org) in die Hände gefallen. Es beschreibt wie man Bürgerbegehren umsetzen kann.

Kannst du dir vorstellen ein Bürgerbegehren für mehr Klimaschutz in deinem Wohnort mitzuplanen? 🤔




