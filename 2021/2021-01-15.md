### 15.1.2021

* [<](/2021/2021-01-08.html)
* [>](/2021/2021-01-22.html)


<div class="wa_dunkelblau"></div>

⚡Hast du Standby-Geräte zuhause, die sich nicht abschalten lassen?⚡


Verpasse ihnen eine 
<br><br>
⏻ Power-Off-Kur
<br><br>
mit einer schaltbaren Steckerleiste.


![einzelner Steckerschalter für die Waschmaschine](/2021/steckdosenschalter-waschmaschine.jpg)
<br><br>
Gibt's auch für einzelne Stecker.

* [<](/2021/2021-01-08.html)
* [>](/2021/2021-01-22.html)