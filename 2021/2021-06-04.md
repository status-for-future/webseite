# 4.6.2021

* [<](/2021/2021-05-27.html)
* [>](/2021/2021-06-09.html)

<div class="wa_flieder"></div>

Soundtrack zum Status heute:
[https://youtu.be/OGm3j3Qbipc](https://youtu.be/OGm3j3Qbipc)

Danke für eure Antworten letzte Woche. Ihr vermisst...<br><br>
📽 Kino<br><br>
🎸🎺🎻🎹 Konzerte<br><br>
🏊 Schwimmbad<br><br>
🤗 Umarmungen<br><br>
(unbeschwert) Freunde treffen🫂<br><br>
und 🍻 Grillpartys natürlich 😅

Was hat sich durch die 🦠 Pandemie noch verändert für dich? 🤔
<br><br>
Welche Veränderung möchtest du auch in Zukunft beibehalten, weil sie dir und deiner Umwelt gut tut? 🦸

Was mir seit letztem Sommer sehr gut tut: Ich sing' immer weiter für die Erde mein Lied.🎹🌍


"Ich sage 'nein' zu all dem Wahnsinn und ich wag' mich hier raus<br><br>
Ich erhebe meine Stimme und ich frage euch laut:<br><br>
Wo sind die Leute da draußen<br><br>
Die an was Besseres glauben?"

"Macht euch nicht so furchtbar klein<br><br>
Wir sind doch längst nicht mehr allein..." 🙂

