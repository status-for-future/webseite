# 25.9.2021

* [<](/2021/2021-09-24.html)
* [>](/2021/2021-10-08.html)

<div class="wa_oliv"></div>

Ich habe ein Video gemacht 😁
<br><br>
[https://youtu.be/9GkQXa-xvsM](https://youtu.be/9GkQXa-xvsM)
<br><br>
[
![Eine Strichfigur mit dem Namen Greta ist zu sehen. Neben ihr ist eine Sprechblase mit einem durchgestrichenen Flugzeug. Man sieht, dass es ein Ausschnitt eines größeren Bildes ist.](/2021/ist-dein-co2-fussabdruck-bloedsinn.jpg)
*"Ist dein CO2-Fußabdruck Blödsinn?"*
](https://youtu.be/9GkQXa-xvsM)

