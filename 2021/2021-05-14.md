# 14.5.2021

* [<](/2021/2021-05-07.html)
* [>](/2021/2021-05-21.html)

<div class="wa_hellblau"></div>

Versprochen: Ich stimme fürs Klima.

Bei der Bundestagswahl wähle ich die Partei, die sich
am überzeugendsten fürs Klima einsetzt.
Denn mit der aktuellen Regierung ist wirksamer
Klimaschutz unmöglich.

Das Klimaversprechen - der Pledge - wird von einem breiten Bündnis
getragen und ist die größte Kampagne der Klimabewegung zu einer Wahl.
Unser Ziel: die Wahl im September zur Klimawahl machen. Mach auch Du
mit und versprich heute, dass Du fürs Klima wählst.

[![Klima Pledge - Meine Stimme für die Zukunft](/2021/klimapledge.jpg)](https://klima-pledge.org/)
