# 16. Juli 2021

* [<](/2021/2021-07-09.html)
* [>](/2021/2021-07-23.html)

<div class="wa_hellblau"></div>

Ich weiß gar nicht wo ich heute anfangen soll... 🤪
<br>
Seit Wochen habe ich das Gefühl es verändern sich jetzt Dinge, die ich mir noch vor fünf Jahren 😮 nicht hätte träumen lassen!
<br>
Deshalb wird der Status heute etwas länger. 🙂

Also macht's euch gemütlich 😊 und schaltet die heutige Hintergrundmusik ein:  
🎵 "Froh dabei zu sein" von Philipp Poisel 🎶  
[https://www.youtube.com/watch?v=qTUv7d1ByJI](https://www.youtube.com/watch?v=qTUv7d1ByJI)


1.<br>
Anfang der Woche habe ich diese Fahne an der katholischen Frauenkirche gesehen:  
![Regenbogenfahne an der Frauenkirche mit der Aufschrift: Wo die Liebe ist, da ist Gott.](/2021/frauenkirche.jpg)

Dort steht: "Wo die Liebe ist, da ist Gott."  
Das hat mich wirklich berührt, weil es ein sehr deutliches öffentliches Bekenntnis dieser Kirche ist.


2.<br>
Heute startet die [SustaiNable Conference](https://nuernberg.digital/programm/programmuebersicht/details-vorschau/sustainable-conference.html)


3.<br>
Die EU hat ein Maßnahmenpaket vorgelegt, wie wir bis 2050 klimaneutral werden:
[https://www.zeit.de/wissen/umwelt/2021-07/eu-klimapolitik-emission-energie-einkommen-klimawandel](https://www.zeit.de/wissen/umwelt/2021-07/eu-klimapolitik-emission-energie-einkommen-klimawandel)

Ein Teil von mir sagt:
<br><br>
🤔 Na ob das reicht?
<br><br>
2 Grad sind viel zu viel! 🥵
<br><br>
Und warum fokussieren deutsche Medien so sehr auf die Auswirkungen für die Autoindustrie!? 😫

Aber ein anderer / größerer Teil von mir schreit laut:
<br><br>
🎉 **HURRAAAA!** 🥳
<br><br>
Endlich reden wir über  
"*wie*"  
und nicht mehr über  
"*ob*"!
<br>
Wie geil ist das denn?? 🤩


4.<br>
Und noch eine kleine 📖 Buchempfehlung, weil "*die da oben*" nicht alles allein machen können und werden. 😉
<br><br>
Mondays for Future von Claudia Kemfert
<br><br>
[https://shop.murmann-verlag.de/de/item/claudia-kemfert-mondays-for-future](https://shop.murmann-verlag.de/de/item/claudia-kemfert-mondays-for-future)

[![Mondays for Future Buch von Claudia Kemfert; Freitag demonstrieren, Am Wochenende diskutieren, Ab Montag anpacken und umsetzen](/2021/mondays-for-future.jpg)](https://shop.murmann-verlag.de/de/item/claudia-kemfert-mondays-for-future)




