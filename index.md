
## Ist dein CO<sub>2</sub>-Fußabdruck Blödsinn?

*TL;DR: Ja!* 😜

Was du **trotzdem** tun kannst und eine ausführlichere Antwort findest du in diesem Video:

[
![Eine Strichfigur mit dem Namen Greta ist zu sehen. Neben ihr ist eine Sprechblase mit einem durchgestrichenen Flugzeug. Man sieht, dass es ein Ausschnitt eines größeren Bildes ist.](/2021/ist-dein-co2-fussabdruck-bloedsinn.jpg)
](https://youtu.be/9GkQXa-xvsM)

Zu dieser Frage habe ich [2021 eine Sketchnote gezeichnet][co2bs-sketchnote] und [ein 📽️ Video gedreht][co2bs-orig].

Hier kannst du das YouTube-Video ansehen: [https://youtu.be/9GkQXa-xvsM][co2bs].

## Bücher

Inzwischen gibt es viele Bücher & Informationsmöglichkeiten. Ein paar liste ich euch hier auf:

- Mondays for Future – Freitag demonstrieren, am Wochenende diskutieren und ab Montag anpacken und umsetzen. Murmann, Hamburg, ISBN 978-3-86774-644-1
- Unsere Welt neu denken: Eine Einladung. Ullstein, Berlin 2020, ISBN 978-3-550-20079-3.
- Was, wenn wir einfach die Welt retten? Handeln in der Klimakrise. Kiepenheuer & Witsch, Köln 2021, ISBN 978-3-462-00201-0.
- Mensch, Erde! Wir könnten es so schön haben. dtv Verlagsgesellschaft, München 2021, ISBN 978-3-423-28276-5.

## Musik, die mir hilft

Manchmal ist diese Krise schwer auszuhalten.

Dann hilft mir persönlich Musik.

Diese Playlists habe ich im Laufe des letzten Jahres erstellt und sie helfen mir, wenn es mir mal nicht so gut geht. Vielleicht helfen sie auch dir?

[🎶 Klimakrise vol. 1: "Zweifel und Zuversicht"][zweifel-und-zuversicht]

[🎶 Klimakrise vol. 2: "Wut"][wut]

[🎶 Klimakrise vol. 3: "Hoffnung"][hoffnung]

[🎶 Klimakrise vol. 4: "Mut"][mut]

## Worum geht's hier?

<div class="clear"></div>

"Keine Lust auf Zwang oder Moralpredigt? 🙄
Trotzdem möchtest **DU** etwas für's Klima tun? ❤️🌍"

Mit diesem Motto habe ich 2020 diese Webseite und den
[Instagram-Kanal @statusforfuture][instainsta] begonnen.

Einmal in der Woche habe ich kleine Erinnerungen 🔔 gepostet welche Möglichkeiten **DU** hast um mitzuhelfen.

Inzwischen poste ich unregelmäßiger, weil ich mich in meinem eigentlich Job für mehr Klimaschutz einsetze.

<!-- Links -->

[co2bs]: https://youtu.be/9GkQXa-xvsM
[co2bs-orig]: /2021/2021-09-25.html
[co2bs-sketchnote]: /2021/2021-07-30.html
[zweifel-und-zuversicht]: https://open.spotify.com/playlist/60g5GeLtjXkZtbSLZdcFTd
<!-- noch nicht gepostet! :o -->
[mut]: https://open.spotify.com/playlist/3u35P70NeKO461ojxXANAF?utm_source=copy-link
<!-- /2022/2022-05-27.html -->
[wut]: https://open.spotify.com/playlist/1qebiSF9FqO82Rq37qqWVL?utm_source=copy-link
<!-- /2022/2022-02-11.html -->
[hoffnung]: https://open.spotify.com/playlist/04fxFq3gwKCbplZoffspGR
<!--/2022/2022-05-06.html -->
[instainsta]: https://www.instagram.com/statusforfuture/

## Für Nerds

> Dich interessiert die Technik hinter dieser Seite? [Ich erklär's Dir gern.](https://gitlab.com/status-for-future/webseite/-/blob/master/technik.md)

