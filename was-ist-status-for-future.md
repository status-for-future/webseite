# About / Was ist status for future?

Diese Webseite ist eine Sammlung von Nachrichten, die Freitags
in meinen WhatsApp-Status und auf Instagram geteilt habe.

Inzwischen poste ich in unregelmäßigen Abständen.

Ich möchte den Menschen in meinem Umfeld jeden Freitag eine kleine Erinnerung dalassen, dass es uns alle gemeinsam braucht.

Vielleicht kann ich ein paar Ideen oder Anregungen geben, die es anderen erleichtern mit dem Thema Klimaschutz in Berührung zu kommen.

Seit dem Lockdown der Corona-Pandemie habe ich über andere Wege
nachgedacht wie ich für mehr Klimaschutz demonstrieren kann ohne
meine Gesundheit allzusehr zu gefährden. 

Natürlich erreiche ich mit meinem WhatsApp Status weniger Menschen als bei einer Demonstration auf der Straße. Aber es schließt sich ja mit anderen Methoden nicht aus.

[👉🏾 Mach mit 👈🏻](CONTRIBUTING.html)

## Kann ich die Nachrichten abonnieren?

Neben meinem WhatsApp Status teile ich die Nachrichten gelegentlich auch auf [Instagram](https://www.instagram.com/statusforfuture/).
