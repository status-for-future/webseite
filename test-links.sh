#!/bin/bash
# -----------------------------------------
# CONFIG
# -----------------------------------------

isExternalCheckEnabled=0
excludePages=("technik.html")

# -----------------------------------------
# /CONFIG
# -----------------------------------------

main() {
	echo "Checking links"
	echo "Configuration:"
	echo "------"
	echo "isExternalCheckEnabled? $isExternalCheckEnabled"
	echo ""
	echo "excludePages?"
	echo "${excludePages[@]}"
	echo "------"

	touch 'checked.out'
	touch 'test-links.out'

	find . -type f -name '*.html' -print0 | while read -r -d $'\0' file
	do
		checkLinksInFileExist "$file"
	done

	echo ''
	echo 'Geprüfte externe Links:'
	echo ''
	cat 'checked.out'

	exitCode=0
	if [ "$(wc -l 'test-links.out' | cut -d\  -f1 )" != '0' ]
	then
		echo ''
		echo 'Es wurden fehlerhafte Links gefunden:'
		echo ''
		cat 'test-links.out'
		
		exitCode=1
	fi

	rm ./*.out

	exit $exitCode
}

checkLinksInFileExist() {
	file="$1"
	f=$(basename "$file")
	d=$(dirname "$file")
	
	if isExcluded "$f" ; then
		return;
	fi

	echo "Checking $file "

	while IFS= read -r href
	do
		if [ "${href:0:4}" = http ]
		then
			checkExternalLink "$href" "$f" $isExternalCheckEnabled;
		else
			checkInternalLink "$href" "$f";
		fi
	done < <(sed -n 's/.*href="\([^"]*\).*/\1/p' "$d/$f")
}

checkExternalLink() {
	href=$1
	file=$2
	skip=$3
	if [ ! "$skip" ] || [ "$skip" == 0 ]
	then
		return
	fi

	# TODO exclude known false-positives 
	# maybe from exclude-file?
	alreadyChecked=$(grep -c "$1" 'checked.out')
	returnCode=$?

	if [ $returnCode -eq 0 ] && [ "$alreadyChecked" != "0" ]; then
		grep "$1" < 'checked.out'
		return;
	fi

	status=$(curl -Is "$1" | grep HTTP/ | head -1 | cut -d\  -f2)
	echo "$1 $status" >> checked.out

	if [[ $status =~ ^[45] ]]
	then
	   echo "file [$file]: href [$1] status [$status]" >> 'test-links.out'
	fi
}

isExcluded() {
	yourValue=$1

	for i in "${excludePages[@]}"
	do
		if [ "$i" == "$yourValue" ] ; then
			return 0
		fi
	done
	return 1
}

checkInternalLink() {
	href=$1
	file=$2
	if [ ! -e "target/$href" ]
	then
		echo "file [$file]:   \"$href\" not found" >> 'test-links.out'
	fi
}

main