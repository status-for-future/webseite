# 12. März 2022

* [<](/2022/2022-03-04.html)
* [>](/2022/2022-03-13.html)

<div class="wa_dunkelblau"></div>

![Ein Fahrrad mit Fahrradanhänger. Darüber steht der Text: "Sprit ist teuer? Mir egal. 😁"](/2022/Screenshot_20220312-195911.jpg)

![4 Beutel mit Einkäufen, ein großer und ein kleiner Getränkekasten stehen in einem Treppenhaus. Darüber steht der Text: "Der ganze Wocheneinkauf passt rein."](/2022/Screenshot_20220312-195914.jpg)
