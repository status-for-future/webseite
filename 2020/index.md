[> 2021 >](/2021)

# 2020

## Dezember

* [25.](/2020/2020-12-25.html)
* [18.](/2020/2020-12-18.html)
* [11.](/2020/2020-12-11.html)
* [4.](/2020/2020-12-04.html)

## November

* [27.](/2020/2020-11-27.html)
* [20.](/2020/2020-11-20.html)
* [13.](/2020/2020-11-13.html)
* [6.](/2020/2020-11-06.html)

## Oktober

* [30.](/2020/2020-10-30.html)
* [23.](/2020/2020-10-23.html)
* [16.](/2020/2020-10-16.html)
* [9.](/2020/2020-10-09.html)
* [2.](/2020/2020-10-02.html)

## September

* [25.](/2020/2020-09-25.html)
* [18.](/2020/2020-09-18.html)
* [11.](/2020/2020-09-11.html)

