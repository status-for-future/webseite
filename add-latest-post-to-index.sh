#!/bin/bash

main() {
	touchAllPosts
	determineTheLatestPost
	exit 0;
}

touchAllPosts() {
	find . -type f -name '[^_]*.out' -print0 | while read -r -d $'\0' file
	do

		if [[ $file =~ ^.*[0-9]{4}\-[0-9]{2}\-[0-9]{2}.* ]]; then
			updateModificationDateBasedOnName "$file"
		fi
	done
}

error() {
	if [ ! "$1" ]; then
		error 'Konnte Fehler nicht melden, weil error ohne parameter aufgerufen wurde.'
		return;
	fi 
	echo "$1"
	exit 1
}

guardFileExists() {
	local sourceOfError=''
	if [ "$2" ]; then
		sourceOfError="$2"
	fi
	if [ ! -f "$1" ]; then
		error "Datei existiert nicht... $sourceOfError"
		return 
	fi
}

updateModificationDateBasedOnName()
{
	local filename="$1"

	guardFileExists "$filename" 'touchFileBasedOnName'

	filebasename=$(basename "$filename" .out);
	dir=$(dirname "$filename")
	touchdate="$(date -d "$filebasename" '+%Y%m%d')1900"
	
	# -m macht modify modtime only
	cp "$dir/${filebasename}.out" "$dir/${filebasename}.dated"
	touch -mt "${touchdate}" "$dir/${filebasename}.dated"

}

determineTheLatestPost() {
	local latestPost=''

	# wir gehen hier davon aus, dass jede Status-Datei 
	# als modification-date das datum hat, was ihr Dateiname
	# beschreibt.

	# automatisch die neueste Datei bestimmen...
	touch -d"-6day" ./start.out
	touch -d"+2day" ./finish.out
	latestPost=$(find . -maxdepth 2 -name '2*.dated' -newer ./start.out ! -newer ./finish.out -printf "%T@\t%p\n" | 
	 sort -r | head -n1|cut -f2)

	if [ ! -f "$1" ]; then
		latestPost=$(find . -maxdepth 2 -name '2*.dated' -printf "%T@\t%p\n" |
			sort -r | head -n1|cut -f2)
	fi
	guardFileExists "${latestPost}" "latest-post.out"
	cp "${latestPost}" "latest-post.out"

	rm 20*/*.dated
}

# actually do this when the script is executed
main
