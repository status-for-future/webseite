# Mach mit!

Dir gefällt die Idee hinter statusforfuture? Hier sind ein paar Ideen, wie Du mitmachen kannst.

## Teilen

Dann teile gern die Nachrichten die du hier siehst.
Kopiere dazu einfach einen Text oder ein Bild und teile es in deinem eigenen WhatsApp Status oder mit ausgewählten Freund\*innen. Du kannst dich auch hier inspirieren lassen und dir selber Nachrichten ausdenken.

## Darüber sprechen

Teile deine Ideen für eine umweltfreundlichere Zukunft und diskutiere 
sie mit deinen Freund*innen.

## Spenden

[Du kannst mir natürlich auch einfach einen Kaffee ☕️ ausgeben, wenn du magst.](https://paypal.me/frie321984)


## Eigene Texte vorschlagen

Du hast eine super Idee für einen Status und möchtest sie mir erzählen? Dann melde Dich auf [gitlab.com][neues-issue] an oder registriere Dich kostenlos und [schreib mir ein Ticket][neues-issue].


[neues-issue]: https://gitlab.com/status-for-future/webseite/-/issues/new
