module.exports = {
  ci: {
    collect: {
      url: ['http://localhost:3000/'],
      startServerCommand: 'python3 -m http.server --directory ./target 3000',
	  startServerReadyPattern: 'serving http',
	  numberOfRuns: 1,
	  settings: {
		  chromeFlags: "--no-sandbox",
		  plugins: ["lighthouse-plugin-webforfuture"],
	  }

    },
    upload: {
      target: 'temporary-public-storage',
    },
  },
};
