TARGETDIR=target
MARKDOWN=./parser/Markdown.pl
SASS=sassc
alle_md_seiten:=$(shell find . -maxdepth 3 -type f  -name "[^_]*.md" -printf '%P\n')
ALLE_SEITEN:=$(patsubst %.md,$(TARGETDIR)/%.html,$(alle_md_seiten))
alle_kompilierten_status_seiten:=$(patsubst %.md, %.out, $(shell find . -maxdepth 3 -type f  -name "2*.md" -printf '%P\n'))
alle_bilder:=$(shell find . -maxdepth 1 -type f \( -name "[^_]*.png" -o -name "[^_]*.jpg" -o -name "[^_]*.svg" -o -name "[^_]*.ico" -o -name "[^_]*.webp"  \) -printf '%P\n')
alle_bilder+=$(shell find ./2020/ -maxdepth 1 -type f \( -name "[^_]*.png" -o -name "[^_]*.jpg" -o -name "[^_]*.svg" -o -name "[^_]*.webp" \) -printf '2020/%P\n')
alle_bilder+=$(shell find ./2021/ -maxdepth 1 -type f \( -name "[^_]*.png" -o -name "[^_]*.jpg" -o -name "[^_]*.svg" -o -name "[^_]*.webp" \) -printf '2021/%P\n')
alle_bilder+=$(shell find ./2022/ -maxdepth 1 -type f \( -name "[^_]*.png" -o -name "[^_]*.jpg" -o -name "[^_]*.svg" -o -name "[^_]*.webp" \) -printf '2022/%P\n')
goal_bilder:=$(patsubst %,$(TARGETDIR)/%,$(alle_bilder))

default: hilfeanzeigen

hilfeanzeigen:
	@echo 'Mit make wird diese Webseite gebaut'
	@echo 'und getestet. Folgende Aufrufmöglichkeiten'
	@echo 'hast Du:'
	@echo ''
	@echo 'make compile'
	@echo '		Erstellt die webseite im Verzeichnis'
	@echo '		./target'
	@echo 'make test'
	@echo '		Führt eine Reihe von Tests aus.'
	@echo 'make serve'
	@echo '		Startet mit python einen lokalen'
	@echo '		Webserver der die Seite bereitstellt.'
	@echo 'make clean'
	@echo '		Löscht alle generierten Daten sowie'
	@echo '     ./target'


compile: latest-post.out \
	$(TARGETDIR)/style.css \
	$(ALLE_SEITEN) \
	$(goal_bilder) \
	$(TARGETDIR)/robots.txt \
	$(TARGETDIR)/humans.txt

$(TARGETDIR)/%.txt: %.txt
	cp $< $@

$(TARGETDIR)/%.png: %.png
	mkdir -p $(@D)
	cp $< $@

$(TARGETDIR)/%.ico: %.ico
	mkdir -p $(@D)
	cp $< $@

$(TARGETDIR)/%.svg: %.svg
	mkdir -p $(@D)
	cp $< $@

$(TARGETDIR)/%.jpg: %.jpg
	mkdir -p $(@D)
	cp $< $@
#	shrink 
#   convert $@ -sampling-factor 4:2:0 -strip -quality 80 -interlace JPEG -colorspace RGB $@
#   convert $@ -resize 660x660\> $@

$(TARGETDIR)/%.webp: %.webp
	mkdir -p $(@D)
	cp $< $@

%.out: %.md
	$(MARKDOWN) $< > $@
	
define compile-page-with-template
	mkdir -p $(@D)
	export template=$(filter %tmpl,$^); cp $$template $@
	sed -i -e "/CONTENT/r $<" "$@"
	sed -i -e "/FOOTER/r _foot.out" "$@"
endef

# Status pages
$(TARGETDIR)/20%index.html: 20%index.out _foot.out overview.tmpl
	$(compile-page-with-template)

$(TARGETDIR)/20%.html: 20%.out _foot.out status.tmpl
	$(compile-page-with-template)

# Insta
$(TARGETDIR)/insta.html: insta.out _foot.out status.tmpl
	$(compile-page-with-template)

# Index page
$(TARGETDIR)/index.html: index.out _foot.out index.tmpl latest-post.out
	$(compile-page-with-template)
	sed -i -e "/LATEST-POST/r latest-post.out" "$@"

# Error pages
$(TARGETDIR)/404.html: 404.out _foot.out error.tmpl
	$(compile-page-with-template)

$(TARGETDIR)/500.html: 500.out _foot.out error.tmpl
	$(compile-page-with-template)

# All other pages with fallback template
$(TARGETDIR)/%.html: %.out _foot.out fallback.tmpl
	$(compile-page-with-template)

# stylesheet
$(TARGETDIR)/style.css: general.scss
	mkdir -p $(@D)
	$(SASS) $< $@


test: compile
	./test-links.sh
	./test-filesizes.sh

serve: compile $(TARGETDIR) 
	@echo 'Use watch -n1 make compile to automatically update'
	cd $(TARGETDIR) && python3 -m http.server

clean:
	rm -rf target
	rm -f ./**/*.out
	rm -f ./*.out

latest-post.out: $(alle_kompilierten_status_seiten)
	@echo "ermittle latest post"
	./add-latest-post-to-index.sh
#	make sure file exists
	ls latest-post.out


.PHONY: compile serve test hilfeanzeigen clean $(TARGETDIR)/index.html latest-post.out
