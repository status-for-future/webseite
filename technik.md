## Technik

### Wie funktioniert diese Seite?

Es handelt sich hier um einen Haufen Markdown-Dateien und ein bisschen CSS &mdash; aus Bequemlichkeit nutze ich SASS.

Es gibt Vorlagen für die einzelnen HTML-Seiten. Diese fügen denn Footer und einen Header ein, damit alle Seiten ein ähnliches Layout bekommen, CSS und favicon vorhanden ist.

1. [index.tmpl](index.tmpl) für die Startseite
1. [status.tmpl](status.tmpl) für alle Seiten die eine "Statusmeldung" enthalten
1. [error.tmpl](error.tmpl) für Fehlerseiten
1. [fallback.tmpl](fallback.tmpl) für alle anderen Seiten

Gebaut wird das ganze mit einer [CI Pipeline auf gitlab.com](.gitlab-ci.yml)
beziehungsweise mit [`make`](Makefile)
gebaut und getestet.

Dort kann man auch sehen welche Schritte nötig sind um die Webseite zu bauen.
