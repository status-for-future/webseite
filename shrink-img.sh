#! /bin/bash

echo 'shrinking "' $1 '"...'
convert $1 \
 -sampling-factor 4:2:0 \
 -strip -quality 80 -interlace JPEG -colorspace RGB \
 -resize 660x660\> 'tmp.jpg'