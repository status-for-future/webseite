
------------------------------

* [About](/was-ist-status-for-future.html)
* [Impressum](/impressum.html)
* [Mitmachen](/CONTRIBUTING.html)
* [🍪 Cookies?](/cookies.html)
* [☕️ ausgeben](https://paypal.me/frie321984)

<div id="wcb" class="wcb carbonbadge wcb-d"></div>
<script src="https://unpkg.com/website-carbon-badges@^1/b.min.js" defer></script>
