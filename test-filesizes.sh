#!/bin/bash

f="$(find ./target/ -type f -size +1M | wc -l)"

if [ "$f" != '0' ]
then
    echo 'Folgende Dateien sind zu groß:'
    find ./target/ -type f -size +1M
    exit 1
fi
